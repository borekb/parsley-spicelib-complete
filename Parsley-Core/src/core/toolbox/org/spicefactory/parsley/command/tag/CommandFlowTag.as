/*
 * Copyright 2011 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
package org.spicefactory.parsley.command.tag {

import org.spicefactory.lib.collection.List;
import org.spicefactory.lib.collection.Map;
import org.spicefactory.parsley.core.command.ManagedCommandFactory;
import org.spicefactory.parsley.core.registry.ObjectDefinitionRegistry;

[XmlMapping(elementName="command-flow")]

/**
 * Tag for command flows declared in MXML or XML configuration.
 * 
 * @author Jens Halm
 */
public class CommandFlowTag extends AbstractCommandParentTag implements NestedCommandTag {
	
	
	/**
	 * @inheritDoc
	 */
	public override function resolve (registry:ObjectDefinitionRegistry) : ManagedCommandFactory {
		var list:List = new List();
		for each (var tag:NestedCommandTag in commands) {
			list.add(new TagFactoryPair(tag, tag.resolve(registry)));
		}
		return new Factory(id, list, registry.context);
	}
	
	
}
}

import org.spicefactory.lib.collection.List;
import org.spicefactory.lib.collection.Map;
import org.spicefactory.lib.command.CommandResult;
import org.spicefactory.lib.command.flow.CommandFlow;
import org.spicefactory.lib.command.flow.CommandLink;
import org.spicefactory.lib.command.flow.CommandLinkProcessor;
import org.spicefactory.lib.command.flow.DefaultCommandFlow;
import org.spicefactory.lib.reflect.ClassInfo;
import org.spicefactory.parsley.command.impl.DefaultManagedCommandProxy;
import org.spicefactory.parsley.command.tag.NestedCommandTag;
import org.spicefactory.parsley.command.tag.link.LinkTag;
import org.spicefactory.parsley.core.command.ManagedCommandFactory;
import org.spicefactory.parsley.core.command.ManagedCommandProxy;
import org.spicefactory.parsley.core.context.Context;

class Factory implements ManagedCommandFactory {
	
	private var id:String;
	private var list:List;
	private var context:Context;
	
	function Factory (id:String, list:List, context:Context) {
		this.id = id;
		this.list = list;
		this.context = context;
	}
	
	public function newInstance () : ManagedCommandProxy {
		var flow:CommandFlow = new DefaultCommandFlow();
		flow.description = id;
		
		var resolvedMap:Map = new Map();
		for each (var tagFactoryPair:TagFactoryPair in list) {
			var com:ManagedCommandProxy = tagFactoryPair.factory.newInstance();
			resolvedMap.put(tagFactoryPair.tag, com);
			resolvedMap.put(com.id, com);
		}
		
		for each (tagFactoryPair in list) {
			var tag:NestedCommandTag = tagFactoryPair.tag;
			var command:ManagedCommandProxy = resolvedMap.get(tag);
			if (tag.links.length == 0) {
				flow.addLink(command, new DefaultLink());
			} else {
				for each (var linkTag:LinkTag in tag.links) {
					var link:CommandLink = linkTag.build(resolvedMap);
					flow.addLink(command, link);
				}
			}
		}
		
		flow.setDefaultLink(new DefaultLink());
		return new DefaultManagedCommandProxy(context, flow, id);
		
	}

	public function get type () : ClassInfo {
		return ClassInfo.forClass(CommandFlow, context.domain);
	}
	
}

class DefaultLink implements CommandLink {

	public function link (result:CommandResult, processor:CommandLinkProcessor) : void {
		processor.complete();
	}
	
}

class TagFactoryPair {
	public var tag:NestedCommandTag;
	public var factory:ManagedCommandFactory;
	
	public function TagFactoryPair(tag:NestedCommandTag, factory:ManagedCommandFactory) {
		this.tag = tag;
		this.factory = factory;
	}
}
